from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_photo(city, state):
    url = f"https://api.pexels.com/v1/search?query={city}+{state}"
    headers = {'Authorization': PEXELS_API_KEY}
    resp = requests.get(url, headers=headers)
    the_json = resp.json()
    pic_dict = {"picture_url": the_json["photos"][0]["src"]["original"]}
    return pic_dict
    


def get_coords(city, state):
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&limit=5&appid={OPEN_WEATHER_API_KEY}"
    resp = requests.get(url)
    return {
        "lat": resp.json()[0]["lat"],
        "lon": resp.json()[0]["lon"],
    }


def get_weather_data(lat, lon):
    url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&units=imperial&appid={OPEN_WEATHER_API_KEY}"
    resp = requests.get(url)
    return {
        "description": resp.json()["weather"][0]["description"],
        "temp": resp.json()["main"]["temp"],
    }
